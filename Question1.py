# Assignment 2
# Omer Zano - 315740118
# Jenya Ivanov - 321858136

from math import sqrt
import random as r
import statistics

TERA = 2 ** 40
GIGA = 2 ** 30
MEGA = 2 ** 20
KILO = 2 ** 10


# Generates random numbers within given range.
def stream_generator1(size, max_number=MEGA):
    return [r.randint(1, max_number) for _ in range(size)]


# Generates numbers similar to eachother but less than size parameter.
def stream_generator2(size=KILO):
    ans = [0]
    for i in range(1, size):
        ans.append(max(i // 2, ans[-1] + r.choice([-1, 0, 1, 1])))

    return ans


# Generates number from 0 to size
def stream_generator3(size=KILO):
    return [i for i in range(size)]


# Generates large numbers.
def stream_generator4(size=KILO):
    return [2**20 for i in range(size)]


def hash_i(number, i):
    return hash(str(number+i))


def question1(stream, quality=9):
    """

    @param stream: Python list of numbers representing a stream
    @param quality: How good this should be.
                    quality=The number of hash functions call is allowed to employ.
    """
    # Variables
    # Initialize list with quality amount of zeroes.
    estimates_array = [0 for i in range(quality)] 

    # Main loop
    for number in stream:
        for i in range(quality):
            hash = hash_i(number, i)
            Ri = trailing_zeroes(hash)
            estimates_array[i] = max(estimates_array[i], 2**Ri)
    
    return generate_output(estimates_array)


# This function counts the binary amount of trailing zeroes within a number (right to left).
def trailing_zeroes(hash: int):
    binary_string = bin(hash)

    # Remove the '0b' Python addon from the string
    first_one = binary_string.find('1') 
    formmated_binary_string = binary_string[first_one:]
    
    # Reverse the string and find the index of the first 1.
    reverse_binary_string = formmated_binary_string[::-1]
    index = reverse_binary_string.find('1')

    return index


# return the true count of distinct elements in a stream.
# note - this is expensive and slows down program
def true_count(stream: list):
    el_set = set()
    for number in stream:
        el_set.add(number)

    return len(el_set)


# calculate error rate, given estimate and true value
def error_rate(estimate: float, true_value: int):
    return (abs((true_value - estimate) / true_value)) * 100 


# generate output as described in docs
def generate_output(estimates_array: list):
    # convert estimates array to matrix for ease of use
    row_len = int(sqrt(len(estimates_array)))
    matrix = []
    for i in range(row_len):
        matrix.append(estimates_array[:row_len])
        estimates_array = estimates_array[row_len:]

    # calculate median for each row
    medians = []
    for row in matrix:
        medians.append(statistics.median(row))

    # average the medians
    return statistics.mean(medians)


# testing
# SIZE = 5000
# input1 = stream_generator1(SIZE)
# input2 = stream_generator2(SIZE)
# input3 = stream_generator3(SIZE)
# input4 = stream_generator4(SIZE)

# true_count1 = true_count(input2)
# estimate1 = question1(input2, 49) 

# error_rate1 =  error_rate(estimate1, true_count1)


# print("[DEBUG] estimate: %d, true count: %d" % (estimate1, true_count1))
# print('[DEBUG] Percent: %d%%' % error_rate1)

# x = stream_generator1(KILO, KILO)
# print(len(x), len(set(x)), x[:20])

# x = stream_generator1(MEGA, GIGA)
# print(len(x), len(set(x)), x[:20])


# x = stream_generator2(KILO)
# print(len(x), len(set(x)), x[:20])


# x = stream_generator1(7 * KILO, 20 * KILO)
# x = stream_generator3(KILO)
# x = stream_generator4(KILO)
# print(len(x), len(set(x)), x[:20])
# print(question1(x, 1), 1)
# print(question1(x, 49), 121)
